package com.haladyj.SS22RS;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ss22RsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ss22RsApplication.class, args);
	}

}
